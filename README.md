# MICCAI Tutorial 2022

Hands-on and any tutorial materials for MICCAI Tutorial 2022 "Embedding deep neural networks within Cognitive AI for machine reasoning and automatic parameter tuning", Website: https://cvib.ucla.edu/miccai22-simplemind_ai/

The tutorial will use SimpleMind Applications built with the SimpleMind Framework (https://gitlab.com/sm-ai-team/simplemind).

## **License**

SimpleMind is licensed under the 3-clause BSD license. For details, please see the LICENSE file.

## **History**

The project started in the [Center for Computer Vision & Imaging Biomarkers (CVIB)](https://cvib.ucla.edu/) at University of California, Los Angeles (UCLA).

### Maintainer

- Matthew S Brown <[mbrown@mednet.ucla.edu](mailto:mbrown%40mednet.ucla.edu)>
- Youngwon Choi <[youngwonchoi@mednet.ucla.edu](mailto:youngwonchoi@mednet.ucla.edu)>
- Wasil Wahi <[mwahianwar@mednet.ucla.edu](mailto:mwahianwar@mednet.ucla.edu)>
- John Hoffman <[johnmarianhoffman@gmail.com](mailto:johnmarianhoffman@gmail.com)>

This package was builded on the [Keras](https://keras.io/) and the [Tensorflow](https://www.tensorflow.org/tutorials) packages.
## **Project status**

---

![https://cvib.ucla.edu/wp-content/uploads/CVIB/Logos/Transparent-CVIB-Logo-e1391555509869.png](https://cvib.ucla.edu/wp-content/uploads/CVIB/Logos/Transparent-CVIB-Logo-e1391555509869.png)
